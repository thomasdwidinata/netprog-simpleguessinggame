import java.util.Map;

final public class Leaderboard{
	private final String key;
	private Integer value;
	
	public Leaderboard(String key, Integer value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public Integer getValue() {
        return value;
    }

    public Integer setValue(Integer value) {
        Integer old = this.value;
        this.value = value;
        return old;
    }
}
