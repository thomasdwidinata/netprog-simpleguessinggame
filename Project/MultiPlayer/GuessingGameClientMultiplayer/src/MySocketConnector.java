

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.Adler32;
import java.util.zip.Checksum;

/**
 *
 * @author thomasdwidinata
 */
public class MySocketConnector {
    
    // Deckare required variables
    private Socket echoSocket;
    private PrintWriter sender;
    private BufferedReader receiver;
    private BufferedReader keyboardInput;
    private String hostAddress;
    private int hostPort;
    
    /**
     * 
     * Constructor automatically connects to the Server. It defines the sockets,
     * checksum method, and buffers
     * 
     * @param hostName      = The server address to be connected
     * @param portNumber    = The server port for sending the data
     * @param checksumPort  = The server port for checksum receiving
     */
    public MySocketConnector(String hostName, int portNumber)
    {
        try{
            // Set required information to connect to the server from the parameter
            this.hostAddress = hostName; 
            this.hostPort = portNumber;
            
            // Try to connect to the Server by creating new instance of Socket object
            echoSocket = new Socket();
            echoSocket.connect(new InetSocketAddress(this.hostAddress, this.hostPort), 15000);
            echoSocket.setKeepAlive(true); // Enable always keep alive!
            // Initialise buffers
            sender = new PrintWriter(echoSocket.getOutputStream(), true);
            receiver = new BufferedReader(new InputStreamReader(echoSocket.getInputStream()));
            keyboardInput = new BufferedReader(new InputStreamReader(System.in));
            new Thread(new PingCentre(echoSocket)).start(); // Create new Thread to send Pings to the server. Making sure that is 
            // still connected.
        } catch (IOException ex) {
            Logger.getLogger(MySocketConnector.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * 
     * Sends string to the connected Server on this instance.
     * 
     * @param data          = String that is going to be send to the Server
     */
    public void sendToServer(String data)
    {
        sender.println(data);
    }
    
    /**
     * 
     * Get server's response by waiting until the buffer gets end line
     * 
     * @return Wait the server response and returns string
     */
    public String getServerResponse()
    {
    	try {
    		return receiver.readLine();
    	} catch (IOException e) {
    		e.printStackTrace();
    	}
    	return null;
    }
    
    /**
     * 
     * Read keyboard input from the user (Client side)
     * 
     * @return Returns the keyboard input in String
     */
    public String readLine()
    {
        try{
            return keyboardInput.readLine();
        } catch (IOException ex) {
            Logger.getLogger(MySocketConnector.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    /**
     * 
     * Get the echoSocket's connection
     * 
     * @return Returns the connection state
     */
    public boolean isConnected()
    {
        return this.echoSocket.isConnected();
    }
    
    /**
     * 
     * Close the connection gracefully
     * 
     */
    public void close()
    {
        try {
        	sender.close();
        	receiver.close();
            echoSocket.close();
        } catch (IOException ex) {
            Logger.getLogger(MySocketConnector.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
