import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/**
 * 
 * The Main Game Server. This class used for generating the numbers and
 * match the numbers guessed by the player.
 * There are special symbols whether the numbers are places correctly or
 * no numbers matching the answers and the correct number position.
 * The special symbols are described below:
 * Legend:
 * O︎ = Numbers are correct, Wrong Order
 * ✕ = No such numbers
 * Y = Correct number and order
 * 
 * 
 * @author thomasdwidinata
 *
 */

public class Game {
	// Declare important variables
	private String numbers = "";
	public static String wrong = "X";
	public static String order = "O";
	public static String correct = "Y";
	public static int maxNum = 8;
	public static int minNum = 3;
	public static int maxGuess = 10;
	private int guessCount = 0;
	private String playerResponse = new String();
	private String playerMark = new String();
	private String gameLogPath = new String();
	private SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	private Date date = new Date();
	private String clientAddress = new String();
	
	// Call resetNumber because it is intended to initialise the data
	public Game(String numbers, String gameLog, String clientAddress) {
		this.clientAddress = clientAddress;
		this.gameLogPath = gameLog;
		resetNumbers(numbers);
	}
	
	// Call resetNumber because it is intended to initialise the data
	public Game(int many, String gameLog, String clientAddress) {
		this.clientAddress = clientAddress;
		this.gameLogPath = gameLog;
		resetNumbers(generator(many));
	}
	
	// The default constructor, only receives gameLog Path and the client address
	public Game(String gameLog, String clientAddress) {
		// Generate random size of the numbers
		this.clientAddress = clientAddress;
		this.gameLogPath = gameLog;
		Random random = new Random();
		int nums = random.nextInt((maxNum - minNum) + 1) + minNum;
		resetNumbers(generator(nums));
	}
	
	// Generates the numbers with specified size
	public String generator(int size) {
		String nums = new String();
		int current = 0;
		Random random = new Random();
		for(int i = 0; i < size; ++i) {
			current = random.nextInt(9);
			while(nums.contains("" + current))
				current = random.nextInt(9);
			nums = nums + current;
		}
		consoleLog("[  i  ] [ %time ] [ %client ] Answer for client %client are as below:\n\t\tAnswer: " +
				nums + "\n\t\tLength: " + nums.length() + "\n");
		return nums;
	}
	
	// Reset the number answer key
	public void resetNumbers(String numbers) {
		this.numbers = numbers;
		this.guessCount = 0;
	}
	
	/**
	 * Check from what the client has answered. When this function triggered, it will automatically
	 * increment the guess count!
	 * @param numbers
	 * @return
	 */
	public String check(String numbers) {
		this.playerResponse = numbers;// Keep track of the player's answer
		String matches = ""; 
		String cache = "";
		if(isGuessLimit()) {// If the guess count has reached its limit, break;
			matches = Game.wrong;
		} else {
			if(numbers.length() != this.numbers.length()) // If received too many argument, which it hopefully won't happend... just give an invalid response
				matches = Game.wrong;
			else {// If the length of the string is correct, check!
				for(int i = 0; i < this.numbers.length(); ++i) {
					cache = "" + numbers.charAt(i);// Get each character number
					if(this.numbers.contains(cache)) { // If the character is in the answer, 
						if(this.numbers.charAt(i) == numbers.charAt(i))// and the character position is the same as the answer
							matches = matches + Game.correct;// Return Correct!, append the correct Symbol
						else
							matches = matches + Game.order;// Else Return Wrong Order!, append the O symbol
					} else
						matches = matches + Game.wrong;// If no match found, then it must be wrong, append the wrong symbol
				}
			}
		}
		++guessCount; // Increment the guess count
		this.playerMark = matches; // Saves the marking
		return matches;
	}
	
	// Check whether the player is win or not by comparing the answer with the response from the client
	public boolean isWin() {
		String log = new String();
		if(this.numbers.equals(this.playerResponse)) {// If both are the same, it wins!
			consoleLog("[  i  ] [ %time ] [ %client ] Hooray! This client wins! With only " + (this.getGuessCount() - 1)
					+ " guesses.\n");
			return true;
		}
		else { // If both are not the same, 
			if (Game.maxGuess - this.getGuessCount() == 0) {
				consoleLog("[  i  ] [ %time ] [ %client ] Oh no, this client has lost the game!");// And has reached its limit of guessing, Lose!
			} else {// Else, display the guess left
				log = "[  i  ] [ %time ] [ %client ] Client answered '"
						+ playerResponse + ",  With the marking : " + this.playerMark + ". The player only have "
						+ (Game.maxGuess - this.getGuessCount()) + " guess chance.\n";
				
				consoleLog(log);
			}
			return false;
		}
	}
	
	private String getTime() { // Get time to write to log
		return this.formatter.format(date);
	}
	
	public int getGuessCount() { // Get how many guess left
		return this.guessCount;
	}
	
	public boolean isGuessLimit() { // Check whether it is guess limit or not
		if(guessCount >= Game.maxGuess)
			return true;
		else 
			return false;
	}
	
	public int numberLength() { // Get the answer length
		return this.numbers.length();
	}
	
	public String getAnswer() {// Get the answer
		return this.numbers;
	}
	
	public boolean consoleLog(String log) { // For logging purpose, it will print to the console and to file for each string given to this function
		boolean ok = false;
		FileWriter fw = null;
		try {
			fw = new FileWriter(gameLogPath, true);
			log = log.replaceAll("%time", getTime());
			log = log.replaceAll("%client", clientAddress);
			System.out.printf(log);
			fw.write(log);
			fw.flush();
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ok;
	}
}
