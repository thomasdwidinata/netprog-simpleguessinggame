# Network Progamming (COSC 1179)

## Assignment 1

### Introduction
The purpose of this assignment is familiarise students into the application of the Java Networking concepts. The program that is currently covered on this assignment is number guessing. There are 2 projects on this assignment, the first one is for the single player server to serve only one players. The server will ask how many numbers from three to eight digits that the client wants to guess. Then the server will generate a number based on what user has specified. The second project is the same as the first project, with difference that the server are able to handle multiple clients (Multi player server). The server should be able to serve many clients as the server will produce threads as the clients comes to the server. But the first player who went to the server will be the one who select the length of the digits that is going to be guessed by itself and other players. The multi-player game play will ask players to give a nickname to be displayed on the leaderboards. At the end of the game, the leaderboard will be displayed to everyone.

The main objective of this assignment is not about implementing the game play, but also how the networking concepts works and the multi-threading works. In order to serve many clients, these concepts are required to code the server application. While on the client side, the required skills are the Java socket connection.

#### Game Description
The game is a simple guessing game. There are two different version of this game. The single player game and multiplayer game. In the beginning of the single player game, the server will ask the player to choose how many numbers that server wants to generate from three to eight numbers. The player only has ten times to guess the number. If the player able to guess the number, they will win, else they will lose the game.
The second version / project is multi-player game. First, player one (First player who enters the lounge) will choose how many digits that the server should to generate that will be used for the answer key for all players. Then the other players who join will be using that generated number, which the length has been determined by the first player who enters the lounge. For more complete specification from the course, please refer to `./Assignment Specification/NetProg-Assign_2018.pdf`. During the game, players are able to forfeit by giving ‘f’ character to the server and immediately the guess count raises to 11, as the specification says. On the end of the game, leaderboards are posted to each people with their nicknames displayed and the number of tries. More information are available on the Project Report `./Assignment_2_report.pdf`.

#### Compiling the Program
The Multiplayer and Single player are separated into 2 different folders. Here are the list of the folder contents and the description of it.

The Multiplayer and Single player are separated into 2 different folders.
There are 2 folders of the combined source code on the root folder. `SinglePlayer` and `MultiPlayer`. Choose which one that you want to compile, and then after entering that directory, you can simply execute the `make` command to compile both the client and the server. Please be noted that there are 2 versions of main class, which is the server and the client. Here are the complete list of the main class:

>`./SinglePlayer`:
* Server: `GuessingGameServerSinglePlayer`
* Client: `GuessingGameClientSinglePlayer`

>`./MultiPlayer`:
* Server: `GuessingGameServerMultiplayer`
* Client: `GuessingGameClientUniversal`

In order to run those class files, you can simply run this command:

>`java <className>`

The `<className>` will be replaced with the above one (Main class Name). Because this is how Java Works! For example if you want to run the multi player server, you can simply use this command

>`java GuessingGameServerMultiplayer 8080`

Note that the `8080` is the port that you want to use for the server.
