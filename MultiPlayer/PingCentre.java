import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * This class handles the ping sending mechanism.
 * It will periodically ping the server to confirm that the server is alive.
 * 
 * @author thomasdwidinata
 *
 */

public class PingCentre implements Runnable{
	
	// Declare important variables
	private Socket socket = null;
	private PrintWriter printer = null;
	private SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	private Date date = new Date();
	
	public PingCentre(Socket socket) {
		this.socket = socket; // Get the client socket
		try {
			// Initialise the writer buffer to the server
			printer = new PrintWriter(socket.getOutputStream(), true);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	@Override
	public void run() { // Will loop for 5 seconds to send the ping message to the server, if unsuccessful, raise exception
		while(true) {
			try {
				Thread.sleep(5000);
				printer.println("[ping]");
			} catch(InterruptedException e) {
				System.err.println("[  !  ] Client has lost its connection to the server... Retrying...");
			}
		}
	}
}
