import java.io.FileWriter;
import java.io.IOException;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

/**
 * 
 * The Main Game Server. This class used for generating the numbers and
 * match the numbers guessed by the player.
 * There are special symbols whether the numbers are places correctly or
 * no numbers matching the answers and the correct number position.
 * The special symbols are described below:
 * Legend:
 * O︎ = Numbers are correct, Wrong Order
 * ✕ = No such numbers
 * Y = Correct number and order
 * 
 * 
 * @author thomasdwidinata
 *
 */

public class Game {
	// Declare important variables
	private static String numbers = "";
	public static String wrong = "✕";
	public static String order = "O";
	public static String correct = "Y";
	public static int maxNum = 8;
	public static int minNum = 3;
	public static int maxGuess = 10;
	public static int minimumPlayers = 2;
	private int guessCount = 0;
	private String playerResponse = new String();
	private String playerMark = new String();
	private String gameLogPath = new String();
	private SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	private Date date = new Date();
	private String clientAddress = new String();
	private ArrayList<Socket> alist = new ArrayList<>();

	public static boolean isReady() { // Get whether the length of the server is sufficient
		if(Game.numbers.isEmpty() || Game.numbers.length() == 0)
			return false;
		else
			return true;
	}
	
	public boolean add(Socket client) { // Add client to keep track of the number of clients
		boolean minimumReached = false;
		
		alist.add(client);
		if(alist.size() > Game.minimumPlayers)
			minimumReached = true;
		
		return minimumReached;
	}
	
	// Call resetNumber because it is intended to initialise the data
	public Game(String numbers, String gameLog, String clientAddress) {
		this.clientAddress = clientAddress;
		this.gameLogPath = gameLog;
		resetNumbers(numbers);
	}
	
	// Call resetNumber because it is intended to initialise the data
	public Game(int many, String gameLog, String clientAddress) {
		this.clientAddress = clientAddress;
		this.gameLogPath = gameLog;
		resetNumbers(generator(many));
	}
	
	public Game(String gameLog, String clientAddress) {
		this.clientAddress = clientAddress;
		this.gameLogPath = gameLog;
	}
	
	public void foul() {
		this.guessCount = 11;
	}
	
	// Generates the numbers with specified size
	public String generator(int size) {
		String nums = new String();
		int current = 0;
		Random random = new Random();
		for(int i = 0; i < size; ++i) {
			current = random.nextInt(9);
			while(nums.contains("" + current))
				current = random.nextInt(9);
			nums = nums + current;
		}
		consoleLog("[  i  ] [ %time ] [ %client ] Answer for this session are as below:\n\t\tAnswer: " +
				nums + "\n\t\tLength: " + nums.length() + "\n");
		return nums;
	}
	
	public void resetNumbers(String numbers) { // Reset the number answer key
		Game.numbers = numbers;
		this.guessCount = 0;
	}
	
	/**
	 * Check from what the client has answered. When this function triggered, it will automatically
	 * increment the guess count!
	 * @param numbers
	 * @return
	 */
	public String check(String numbers) { 
		this.playerResponse = numbers; // Keep track of the player's answer
		String matches = ""; 
		String cache = "";
		if(isGuessLimit()) { // If the guess count has reached its limit, break;
			matches = Game.wrong;
		} else {
			if(numbers.length() != Game.numbers.length()) // If received too many argument, which it hopefully won't happend... just give an invalid response
				matches = Game.wrong;
			else { // If the length of the string is correct, check!
				for(int i = 0; i < Game.numbers.length(); ++i) {
					cache = "" + numbers.charAt(i); // Get each character number
					if(Game.numbers.contains(cache)) { // If the character is in the answer, 
						if(Game.numbers.charAt(i) == numbers.charAt(i)) // and the character position is the same as the answer
							matches = matches + Game.correct; // Return Correct!, append the correct Symbol
						else
							matches = matches + Game.order; // Else Return Wrong Order!, append the O symbol
					} else
						matches = matches + Game.wrong; // If no match found, then it must be wrong, append the wrong symbol
				}
			}
		}
		++guessCount; // Increment the guess count
		this.playerMark = matches; // Saves the marking
		return matches;
	}
	
	public static int getNumberLength() { // Get the length of the answer
		return Game.numbers.length();
	}
	
	public boolean isWin() { // Check whether the player is win or not by comparing the answer with the response from the client
		String log = new String();
		if(Game.numbers.equals(this.playerResponse)) { // If both are the same, it wins!
			consoleLog("[  i  ] [ %time ] [ %client ] Hooray! This client wins! With only " + (this.getGuessCount() - 1)
					+ " guesses.\n");
			return true;
		}
		else { // If both are not the same, 
			if (Game.maxGuess - this.getGuessCount() == 0) { // And has reached its limit of guessing
				consoleLog("[  i  ] [ %time ] [ %client ] Oh no, this client has lost the game!"); // It Lose!
			} else { // Else, display the guess left
				log = "[  i  ] [ %time ] [ %client ] Client answered '"
						+ playerResponse + ",  With the marking : " + this.playerMark + ". The player only have "
						+ (Game.maxGuess - this.getGuessCount()) + " guess chance.\n";
				
				consoleLog(log);
			}
			return false;
		}
	}
	
	private String getTime() { // Get time to write to log
		return this.formatter.format(date);
	}
	
	public int getGuessCount() { // Get how many guess left
		return this.guessCount;
	}
	
	public boolean isGuessLimit() { // Check whether it is guess limit or not
		if(guessCount >= Game.maxGuess)
			return true;
		else 
			return false;
	}
	
	public String getAnswer() {// Get the answer
		return this.numbers;
	}
	
	public boolean consoleLog(String log) { // For logging purpose, it will print to the console and to file for each string given to this function
		boolean ok = false;
		FileWriter fw = null;
		try {
			fw = new FileWriter(gameLogPath, true);
			log = log.replaceAll("%time", getTime());
			log = log.replaceAll("%client", clientAddress);
			System.out.printf(log);
			fw.write(log);
			fw.flush();
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ok;
	}
}
