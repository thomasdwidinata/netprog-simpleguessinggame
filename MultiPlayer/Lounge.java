import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

/**
 * 
 * This class is used for managing the clients. The purpose is to provide global variable across Threads.
 * A thread contains a socket that handles one client (player). This class will keep the information
 * about the threads so it will automatically able to manage the game status either it is running or not.
 * 
 * @author thomasdwidinata
 *
 */

public class Lounge {
	
	// The map is like the associative array, just like in PHP Array and JSON.
	// The Thread is to save the thread while the boolean indicates that the player has finished playing the game
	private static Map<Thread, Boolean> players; 
	private static int minimumPlayers; // For caching the minimum players got from Game.minimumPlayer
	private static GAME_STATUS started; // The global status of the game, all threads should monitor this thing
	// For leaderboard purpose. It uses priority queue to automatically sort the lowest guess to highest
	private static PriorityQueue<Leaderboard> leaderboard;
	
	// The enum values of the Game Status
	public enum GAME_STATUS{
		WAITING,
		STARTED,
		FINISHED
	}
	
	public Lounge(int minimumPlayers) { // At the initialisation of Lounge, it will reset into the initial value
		reset(minimumPlayers);
	}
	
	public static void reset(int minimumPlayers) {
		Lounge.minimumPlayers = minimumPlayers; // Get the minimum player from the constructor, or modular purpose
		Lounge.started = GAME_STATUS.WAITING; // At the first game, it will wait until many players join the game
		Lounge.leaderboard = new PriorityQueue<Leaderboard>(10, new Comparator<Leaderboard>() {

			@Override // Create a comparison function to compare the Leaderboard Object
			public int compare(Leaderboard o1, Leaderboard o2) {
				if(o1.getValue() < o2.getValue()) return -1;
				if(o1.getValue() > o2.getValue()) return 1;
				return 0;
			}
			
		});
		Lounge.players = new HashMap<>(); // Create new object of Map
	}
	
	public static boolean add(Thread thread, boolean start) { // Add new client to the lounge (Map)
		Lounge.players.put(thread, false);
		if(start)
			thread.start();
		if(Lounge.players.size() >= Lounge.minimumPlayers) { // Check whether there are minimum clients are in the 
			Lounge.started = GAME_STATUS.STARTED;
			return true;
		}
		else { // If no 
			Lounge.started = GAME_STATUS.WAITING;
			return false;
		}
	}
	
	public static int getPlayersCount() {
		return Lounge.players.size();
	}
	
	public static void start() {
		Lounge.started = GAME_STATUS.STARTED;
	}
	
	public static void finish(Thread thread, String nickname, int numOfGuesses) {
		boolean gameFinished = false;
		Lounge.players.replace(thread, true);
		Lounge.leaderboard.add(new Leaderboard(nickname, numOfGuesses));
		for(Boolean value : Lounge.players.values()) {
			if(value) {
				gameFinished = true;
			} else {
				gameFinished = false;
				break;
			}
		}
		if(gameFinished)
			Lounge.started = GAME_STATUS.FINISHED;
	}
	
	public static void stop() {
		Lounge.started = GAME_STATUS.FINISHED;
	}
	
	public static GAME_STATUS getGameStatus() {
		return Lounge.started;
	}
	
	public static PriorityQueue<Leaderboard> getLeaderboard() {
		return Lounge.leaderboard;
	}
	
	public static void removeMe(Thread t) {
		Lounge.players.remove(t);
	}
}
