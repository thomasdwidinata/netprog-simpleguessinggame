import java.util.Scanner;

/**
 *
 * Guessing Game (Server) [Universal]
 * v1.0
 *
 * A client side application that will connects to GuessingGameServer. There are no interface here because
 * all user interface will be provided by the server.
 *
 * @author thomasdwidinata
 */
public class GuessingGameClientUniversal {

    public static void main(String[] args) {

    	// Declare required variables
        String hostName = new String();
        String data = new String(); // For saving the input from keyboard
        String serverResponse = new String(); // For saving the response from the Server
        String serverResponseTrucated = new String();
        int portNumber = 0;
        boolean isPlaying = true;
    	Scanner scan = new Scanner(System.in);

        // Check whether the argument given is 3, because the program receives 3 arguments, host address, port number, and checksum port
    	try {
    		switch(args.length) { // Check the argument length
        	default: // If invalid argument received, just ask the client where to connect!
        		System.out.println("Invalid argument specified.");
        		System.out.println("Usage: java GuessingGameClientUniversal <host name> <port number>");
        		System.out.printf("Please specify the host name that you want to connect to: ");
        		hostName = scan.nextLine();
        		System.out.printf("Please specify the host server port: ");
        		portNumber = scan.nextInt();
        		break;
        	case 2: // If valid, then assign it!
        		hostName = args[0];
        		portNumber = Integer.parseInt(args[1]);
        		break;
        	}
    	} catch(Exception e) {
    		e.printStackTrace();
    	}

        // Connecting to the server by creating new instance of my class
        System.out.printf("Connecting to hostname '%s' with port '%s'...\n", hostName, portNumber);
        MySocketConnector socket = new MySocketConnector(hostName, portNumber); // Our socket object, that has many API that can be called upon object creation

        // If socket is not connected, don't continue!
        if(!socket.isConnected())
        {
            System.err.printf("Unable to connect to the Server '%s':'%s'\n", hostName, portNumber);
            System.exit(1);
        }

        // If socket is connected to the server, continue!
        System.out.printf("Connected to '%s:%s'!\n", hostName, portNumber);

        while(data != null && isPlaying) // While server does not give any null data, the connection might be safe
        {
        	serverResponse = socket.getServerResponse() + "\n"; // Wait until server gives response
        	if(serverResponse.equals("\n")) { // If did not get any response, just notify the user
                System.out.println("Did not get any response from Server... Retrying...");
                continue;
        	}
        	serverResponseTrucated = serverResponse.substring(serverResponse.indexOf("]") + 1, serverResponse.length());
        	switch(serverResponse.substring(0, serverResponse.indexOf("]")+1)) {
        	case "[  !  ]": // Warning! Invalid number length!
        		break;
        	case "[  i  ]": // The game info and turn, and it will read and send the data
        		System.out.printf(serverResponseTrucated);
        		data = socket.readLine(); // Read from keyboard input
                socket.sendToServer(data); // Send the data to the server, with the checksum
        		break;
        	case "[  ✕  ]": // Error occured
        		System.out.printf("Something happened on the server. This is the message %s\n", serverResponseTrucated);
        		isPlaying = false;
        		break;
        	case "[  g  ]": // Game over
        		isPlaying = false;
        		break;
        	case "[  w  ]": // Winner!
        		System.out.printf(serverResponseTrucated);
        		isPlaying = false;
        		break;
        	case "[  p  ]": // Just Print it, later there will be turn to play
        		System.out.printf("%s", serverResponseTrucated);
        		break;
        	case "[  l  ]": // You Lose!
        		System.out.printf("%s", serverResponseTrucated);
        		isPlaying = false;
        		break;
        	case "[ping]": // For server acknowledgement
        		break;
        	case "[wait]": // Wait for the lounge to reach the minimum players...
        		System.out.printf(serverResponseTrucated);
        		break;
        	case "[ask]": // Ask whether to play again or not
        		while(!(data.equals("q") || data.equals("p"))) {
        			System.out.printf("Do you still want to play again? Enter 'p' to play again and wait for the next round. 'q' for exit:");
        			data = socket.readLine();
        		}
        		if(data.equals("q"))
        			isPlaying = false;
        		else
        			System.out.println("Playing again...");
        		break;
        	case "[ignore]": //To be ignored
        		break;
    		default: // Unknown code
    			System.out.printf("Unknown response received! This is the full message : %s\n", serverResponse);
    			isPlaying = false;
    			break;
        	}
        }
        System.out.println("Good bye~...");
        socket.close(); // Close connection gracefully
        System.exit(0);
    }
}
